Retake Site Notice
       Addon plugin for the Splewis retake mod. Once installed it can show which bombsite the
       bomb is planted on by chat, hint, and HUD message. 

/* CONVARS */
sm_notice_chat_enable(Toggle enables/disables chat messages. set to 1 by default.)
sm_notice_chat_number(Sets how many times bomb site prints to chat. Set to 3 by default)
sm_notice_chat_time(How many seconds between bomb site chat notices. Set to 4 by default)
sm_notice_hint_number(Sets how many times the hint prints to chat. Set to 3 by default)
sm_notice_hint_time(How many seconds between hint notices. Set to 5 by default)
sm_notice_hint_enable(Toggle enables/disables hint messages. Set to 1 by default.)
sm_notice_hud_enable(Toggle enables/disables HUD message. Set to 1 by default)

/* INSTALL */
Copy retake-site-notice.smx to the plugin folder. Do not set parameters that cause the
timers to go into the next round. This will cause unwanted behavior, and maybe incorect info.