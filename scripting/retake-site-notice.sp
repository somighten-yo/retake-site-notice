/**
*
* Retake site notifier for the Splewis plugin 
* By: Somighten
* https://bitbucket.org/somighten-yo/retake-site-notice/src
*
* Displays multipule notices OnSitePicked
* Version 0.4
*
**/

#include <sourcemod>
#include <sdktools>
#include <cstrike>
#include <retakes>
#include <timers>

#pragma semicolon 1

public Plugin myinfo = {
	name = "Retake Site Notice",
	author = "Somighten",
	description = "Display Site Notices",
	version = "0.4",
	url = "https://bitbucket.org/somighten-yo/retake-site-notice/src"
};

/* Convars for chat settings */
ConVar g_chat_enable;
ConVar g_chat_number;
ConVar g_chat_time;
/* Convars for hint settings */
ConVar g_hint_enable;
ConVar g_hint_number;
ConVar g_hint_time;
/* Convars for hud settings */
ConVar g_hud_enable;
/* Counters for number convars */
int countChat = 0;
int countHint = 0;

public void OnPluginStart()
{
	LoadTranslations("sitenotice.phrases");
	HookEvent("round_end", EventRoundEnd);
	
	g_chat_enable = CreateConVar("sm_notice_chat_enable", "1", "0/1 Does plugin print to chat");
	g_chat_number = CreateConVar("sm_notice_chat_number", "3", "How many times does chat print");
	g_chat_time = CreateConVar("sm_notice_chat_time", "4", "How many seconds apart are chat notices");

	g_hint_number = CreateConVar("sm_notice_hint_number", "3", "How many times does hint print");
	g_hint_time = CreateConVar("sm_notice_hint_time", "5", "How many seconds apart are hint notices");
	g_hint_enable = CreateConVar("sm_notice_hint_enable", "1", "0/1 Does plugin print hint");

	g_hud_enable = CreateConVar("sm_notice_hud_enable", "1", "does hud message show");
	
}
/* Reset timer counters on round end */
public Action EventRoundEnd(Event event, const char[] name, bool dontBroadcast)
{
	countChat = 0;
	countHint = 0;
}
/* Called from Splewis retake */
public void Retakes_OnSitePicked(Bombsite& site)
{
	/* Variable for the timer */
	float cSec = g_chat_time.FloatValue;
	float hSec = g_hint_time.FloatValue;
	/* Set timer counters */
	countChat = g_chat_number.IntValue;
	countHint = g_hint_number.IntValue;
	/* Only display enabled notices */
	if(g_hint_enable.IntValue == 1) CreateTimer(hSec, SiteHint,  site, TIMER_REPEAT);
	if(g_chat_enable.IntValue == 1) CreateTimer(cSec, SiteChat, site, TIMER_REPEAT);
	if(g_hud_enable.IntValue == 1) CreateTimer(4.0, SiteHud, site);
}
/* Chat timer */
public Action SiteChat(Handle timer, any site)
{
	if(countChat == 0) return Plugin_Stop;
	if(site == BombsiteA) Retakes_MessageToAll("%t", "ChatA");
	if(site == BombsiteB) Retakes_MessageToAll("%t", "ChatB");
	countChat --;
	return Plugin_Continue;
}
/* Hint timer */
public Action SiteHint(Handle timer, any site)
{
	if(countHint == 0) return Plugin_Stop;
	if(site == BombsiteA) PrintHintTextToAll("%t", "SiteHintA");
	if(site == BombsiteB) PrintHintTextToAll("%t", "SiteHintB");
	countHint --;
	return Plugin_Continue;
}
/* Hud timer */
public Action SiteHud(Handle timer, any site)
{
	Handle HudSite = CreateHudSynchronizer();
	SetHudTextParams(-1.0, 0.2, 3.5, 255, 0, 0, 255, 1, 1.0, 1.0, 1.0);
	/* Show HUD message to all clients */
	for (int i = 1; i <= MaxClients; i++)
	{
		if(site == BombsiteA && IsClientInGame(i)) ShowSyncHudText(i, HudSite, "%t", "HudA");
		if(site == BombsiteB && IsClientInGame(i)) ShowSyncHudText(i, HudSite, "%t", "HudB");
	}
	CloseHandle(HudSite);
}
